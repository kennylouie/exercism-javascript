const transcription = {
    'C': 'G',
    'G': 'C',
    'A': 'U',
    'T': 'A',
};

function toRna(oligonucleotide) {

    if (oligonucleotide === '') {
        return '';
    };

    const polymerase = (daughterStrand, template) => {
        if (transcription[template]) {
            return daughterStrand + transcription[template];
        };
        throw new Error('Invalid input DNA.');
    };

    const Rna = [...oligonucleotide].reduce(polymerase, '');

    return Rna;

};

module.exports = {
    toRna,
};