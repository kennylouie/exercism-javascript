function reverseString(string) {

    const stringLength = string.length;
    const reverser = (newString, current, index, array) => {
        return newString + array[stringLength-index-1];
    };

    return [...string].reduce(reverser, '');
};

module.exports = reverseString;