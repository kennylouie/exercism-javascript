//
// This is only a SKELETON file for the 'Hello World' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

class HelloWorld {
  hello() {
    let greeting = 'Hello, World!';
    return greeting;
  };
};

export default HelloWorld;
