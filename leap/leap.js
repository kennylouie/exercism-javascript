function isLeap(year) {

    if (isDivisible(year, 4)) {

        if (isDivisible(year, 100)) {

            if (isDivisible(year, 400)) {
                return true;
            };

            return false;
        };

        return true;
    };

    return false;
};

function isDivisible(dividend, divider) {
    return (dividend % divider === 0) ? true : false;
};

module.exports = {
    isLeap,
};